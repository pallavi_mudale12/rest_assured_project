# Rest_Assured_Project

In this framework I have multiple components like:-
-Test Driving Packages
-Test Script
-Common Function
- [API Related Common Functions]          - [Utilities]
-Data/Variable File
-Libraries
-Reports
first we have the two driving mechanism one is staic and dynamic driver class in our Test Driving packages. In which we are calling the test scripts in static class and calling them in our main method for execution and apart from that we have created a new dynamic class as well in which we are able to fetch the test cases to be executed from the user from an excel file and dynamically call the test scripts on run time and this is being done by "java.lang.reflect".
Test Script in this we have all the available test scripts those needs to be created.
In common methods, we have 2 categories
API related common functions which are used to trigget the API, extract the responseBody & extract the response status code.
in Utilities we have class to create the directory, if it doesn't exist in the project for test log files and if it does exist than to delete the old one and create the new one for log files. Also we have another utility from which all the endpoint, requestBody, responseBody can be stored in the notepad/text file. And another utility to read the value/data from an excel file.
Data/Variable = We are using the excel files to input the data to our framework or requestBody parameters to input from excel files.
Libraries = we are using RestAssured, Apache-Poi, testng libraries and dependency management of libraries are being managed by maven repository.
Reports = We are using Allure reports and Extent reports.


