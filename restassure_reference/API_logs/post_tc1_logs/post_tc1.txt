Endpoint is :https://reqres.in/api/users

Request Body is :{
    "name": "morpheus",
    "job": "leader"
}

Response Body is :{"name":"morpheus","job":"leader","id":"79","createdAt":"2023-12-12T14:03:25.478Z"}