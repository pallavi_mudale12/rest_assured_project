package API_common_methods;

import static io.restassured.RestAssured.given;

public class common_method_handle_api {
	
	public static int post_statusCode(String requestBody, String endpoint) {
		
		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}
	public static String post_responseBody(String requestBody, String endpoint) {
		
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().response().asString();
		return responseBody;
		}

	
//----------------put----------------------
  public static int put_statusCode(String requestBody, String endpoint) {
		
		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}
	public static String put_responseBody(String requestBody, String endpoint) {
		
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;
	}

//--------------------patch----------------------
  public static int patch_statusCode(String requestBody, String endpoint) {
	
	int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
			.then().extract().statusCode();
	return statusCode;
}

public static String patch_responseBody(String requestBody, String endpoint) {
	
	String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
			.then().extract().response().asString();
	return responseBody;
}

/////-------------get-----------
public static int get_statusCode(String endpoint) {
	
	int statusCode = given().when().get(endpoint)
			.then().extract().statusCode();
	return statusCode;
}

public static String get_responseBody(String endpoint) {
	
	String responseBody = given().when().get(endpoint)
			.then().extract().response().asString();
	return responseBody;

}
}