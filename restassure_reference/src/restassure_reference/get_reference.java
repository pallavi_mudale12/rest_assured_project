package restassure_reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class get_reference {

	public static void main(String[] args) {
		int expected_id[]= {1,2,3,4,5,6};
		String expected_firstname[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
		String expected_lastname[]= {"Bluth","Weaver","Wong","Holt","Morris","Ramos"};
		String expected_email[]= {"george.bluth@reqres.in","janet.weaver@reqres.in","emma.wong@reqres.in","eve.holt@reqres.in","charles.morris@reqres.in","tracey.ramos@reqres.in"};
		
		//step1:declare base url
		 RestAssured.baseURI = "https://reqres.in";
		 
		 //step2:trigger the api and feching the response
		  String responsebody=given().when().get("api/users?page=1").then().extract().response().asString();
		 System.out.println("response body is: " +responsebody);
		 
		 JsonPath jsp_res=new JsonPath(responsebody);
		 JSONObject array_res=new JSONObject(responsebody);
	     JSONArray dataarray = array_res.getJSONArray("data");
			System.out.println(dataarray);
			
			int count = dataarray.length();
			System.out.println(count);
			for(int i=0; i<count; i++) {
				int res_id=dataarray.getJSONObject(i).getInt("id");
				System.out.println(res_id);
				int exp_id=expected_id[i];
				
				String res_firstname=dataarray.getJSONObject(i).getString("first_name");
				String res_lastname=dataarray.getJSONObject(i).getString("last_name");
				String res_email=dataarray.getJSONObject(i).getString("email");
				
				String exp_firstname=expected_firstname[i];
				String exp_lastname=expected_lastname[i];
				String exp_email=expected_email[i];
				
				Assert.assertEquals(res_id, exp_id);
				Assert.assertEquals(res_firstname, exp_firstname);
				Assert.assertEquals(res_lastname, exp_lastname);
				Assert.assertEquals(res_email, exp_email);
		 }
			}
}
