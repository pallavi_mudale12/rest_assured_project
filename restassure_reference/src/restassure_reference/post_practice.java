package restassure_reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class post_practice {

	public static void main(String[] args) {
		// step1:declare url
		RestAssured.baseURI = "https://reqres.in/";
		// step2:configure requestbody and trigger the api
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String responseBody = given().header("Content-Type", "application/json")
				.body(requestBody).when()
				.post("api/users").then().extract().response().asString();
		System.out.println("responseBody is: "+responseBody);
		//step3:create an object of JsonPath to parse requestBody then the responseBody
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		}

}
