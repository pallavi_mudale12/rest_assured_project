package restassure_reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class POST_reference1 {

	public static void main(String[] args) {
		RestAssured.baseURI = "https://reqres.in/";
		String reqestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json").body(reqestbody).when()
				.post("api/users").then().extract().response().asString();
		System.out.println("responsebody is :" + responsebody);

		JsonPath jsp_req = new JsonPath(reqestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);

	}

}
