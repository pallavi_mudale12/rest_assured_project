package restassure_reference;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class soap_reference {

	public static void main(String[] args) {
		//step1:declare base url
		RestAssured.baseURI="https://www.dataaccess.com";
		
		//step2:declare requestBody
		String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
		//step3:trigger the api and fetch the responsebody
		String responseBody =given().header("Content-Type","text/xml; charset=utf-8").body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso").then()
				.extract().response().getBody().asString();
		//step4:print responseBody 
		System.out.println(responseBody);
		
		//step5:extract the responseBody parameter
		XmlPath Xml_res=new XmlPath(responseBody);
		String res_tag=Xml_res.getString("NumberToWordsResult");
		System.out.println(res_tag);
		
		//step6:validate responseBody
		Assert.assertEquals(res_tag,"five hundred ");
		
		}

}
