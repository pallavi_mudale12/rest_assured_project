package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class Patch_request_repository {
	public static String patch_request_tc1() throws IOException {
		ArrayList<String> data = Excel_data_extractor.Excel_data_reader("test_data", "patch_api", "patch_tc3");
		//System.out.println(Data);
		String name=data.get(1);
		String job=data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;
	
		
	}

}
