package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class Post_request_repository {
	
	public static String post_request_tc1() throws IOException {
		ArrayList<String> data=Excel_data_extractor.Excel_data_reader("test_data", "post_api", "post_tc1");
		//System.out.println(data);
		String name=data.get(1);
		String job=data.get(2);
		
		String requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestBody;
	}

}
