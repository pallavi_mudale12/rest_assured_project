package Driver_package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class Dynamic_driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> tc_execute=Excel_data_extractor.Excel_data_reader("test_data","Test_cases","tc_name");
		System.out.println(tc_execute);
		int count=tc_execute.size();
		for(int i=1;i<count;i++) {
			String tc_name=tc_execute.get(i);
			System.out.println(tc_name);
			
			// Call the testcaseclass on runtime by using java.lang.reflect package
			Class<?> test_class=Class.forName("Test_Package."+tc_name);
			
			// Call the execute method belonging to test class captured in variable tc_name by using java.lang.reflect.method class
			Method execute_method=test_class.getDeclaredMethod("executor");
			
			// Set the accessibility of method true
			execute_method.setAccessible(true);
			
			// Create the instance of testclass captured in variable name testclassname
			Object instance_of_test_class=test_class.getDeclaredConstructor().newInstance();
			
			//Execute the test script class fetched in variable test_class
			execute_method.invoke(instance_of_test_class);
			}
	}

}
