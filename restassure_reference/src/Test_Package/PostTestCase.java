package Test_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.common_method_handle_api;
import Endpoint.Post_endpoint;
import Request_repository.Post_request_repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.restassured.path.json.JsonPath;

public class PostTestCase extends common_method_handle_api{
	@Test
	public static void executor() throws IOException {
		    File log_dir=Handle_directory.create_log_directory("post_tc1_logs");
		    String requestBody = Post_request_repository.post_request_tc1();
			String endpoint = Post_endpoint.post_endpoint_tc1();
			for (int i=0; i<5; i++) {
			
			int statusCode = post_statusCode(requestBody,endpoint);
			System.out.println(statusCode);
			
			if (statusCode == 201) {
			
		    String responseBody = post_responseBody(requestBody,endpoint);
			System.out.println(responseBody);
			Handle_api_logs.evidence_creator(log_dir,"post_tc1",endpoint,requestBody,responseBody);
			PostTestCase.validator(requestBody,responseBody);
			break;
			}else {
				System.out.println("expected statuscode not found,hence retrying");
			}
			}
	}
	
	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);
		
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("createdAt");
		res_createdat = res_createdat.substring(0, 11);
		
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, expected_date);
	}
			}


